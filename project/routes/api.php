<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::any('test', function () {
        return response()->json(["message" => "WORKING FINE"]);
    });

});


//ARTICLE ROUTES
Route::prefix("articles")->as("article.")->group( function () {
   Route::get("", "ArticleController@index")->name("list");
   Route::post("", "ArticleController@store")->middleware("auth:api")->name("create");
   Route::get("search", "ArticleController@filter")->name("filter");
   Route::get("/{id}", "ArticleController@show")->name("get");
   Route::put("/{id}", "ArticleController@update")->middleware("auth:api")->name("update");
   Route::delete("/{id}", "ArticleController@destroy")->middleware("auth:api")->name("delete");
   Route::post("/{id}/rating", "ArticleController@rating")->name("delete");
});
