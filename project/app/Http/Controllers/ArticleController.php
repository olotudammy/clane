<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Crystoline\LaraRestApi\RestApiTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    use RestApiTrait;

    public function __construct()
    {
        //dd(\request()->all());
        //dd(\request()->getMethod());
        //$this->middleware("auth:api")->only([]);
    }

    /**
     * This sets the model to be used with the RestApiTrait
     * @return string
     */
    public static function getModel(): string
    {
        return Article::class;
    }


    /**
     * This sets the validation rules for creating a single article
     * @return array
     */
    public static function getValidationRules(): array
    {
        return [
            //"user_id" => "required|exists:users,id",
            "title" => "required|unique:articles,title",
            "description" => "required",
            "content" => "required",
        ];
    }


    /**
     * This sets the validation rules for updating a single article
     * @param Model $model
     * @return array
     */
    public static function getValidationRulesForUpdate(Model $model): array
    {

        return [
            //"user_id" => "required|exists:users,id",
            "title" => "required|unique:articles,title,".$model->id,
            "description" => "required",
            "content" => "required",
        ];

    }

    /**
     * This is called for futher check before storing
     * @param Request $request
     * @return bool
     */
    public function beforeStore(Request $request): bool
    {
        $request->merge([
            "user_id" => Auth::id()
        ]);
        return true;
    }

    /**
     * This method is called before all articles are listed. Index hook
     * @param $data
     */
    public function beforeList($data)
    {
        $data->load("user", "ratings");
    }


    /**
     * Called before showing a single article
     * @param $data
     */
    public function beforeShow($data)
    {
        $data->load("user", "ratings");
    }

    /**
     * Called immediately an Article instance is saved
     * @param Request $request
     * @param $data
     * @return bool
     */
    public function afterStore(Request $request, $data): bool
    {
        $data->load("user", "ratings");
        return true;
    }

    /**
     * Called after updating Article instance
     * @param Request $request
     * @param $data
     * @return bool
     */
    public function afterUpdate(Request $request, $data): bool
    {
        $data->load("user", "ratings");
        return true;
    }

    /**
     * Called after updating
     * @param Request $request
     * @return bool
     */
    public function beforeUpdate(Request $request): bool
    {
        $request->merge([
            "user_id" => Auth::id()
        ]);
        return true;
    }

    /**
     * This is used to rate an article
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function rating(Request $request, $id)
    {
        $request->validate([
            "number_of_stars" => "required|numeric"
        ]);
        $article = Article::query()->where("id", $id)->first();
        if (! isset($article)) {
            return response()->json(["message => Invalid Articel Id supplied"]);
        }

        $rating = $article->ratings()->create([
            "number_of_stars" => $request->number_of_stars
        ]);

        return response()->json($rating->load("article"), self::$STATUS_CODE_DONE);
    }


    /**
     * This is used to search articles
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function filter(Request $request)
    {
        $search = $request->serach;
        $article = Article::query()->where("title", "LIKE", "%". $search. "%")
                                    ->orWhere("description", "LIKE", "%".$search. "%")
                                    ->get();

        return response()->json($article->load("user", "ratings"));

    }
}
