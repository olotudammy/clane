

## CLANE 

> **Note:** Clane Laravel API Article Test.

Follow this steps to set up the application after cloning the repository:

- Make sure Docker is installed on your machine.
- Run "cd project" from the root folder
- Run "composer install" from project folder to get all dependencies
- Run "cd .." from the project folder to return to the root folder
- Run "docker-compose up -d --build" from the root folder
- Run "docker exec -it  article_app_1 bash" from the root folder
- Run "php artisan migrate" to run the migrations files
- Project would be available on "http://localhost:8080/"
- Postman Collection for test is available in the postman folder.


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).